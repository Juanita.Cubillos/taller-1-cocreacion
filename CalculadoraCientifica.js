class CalculadoraCientifica extends Calculadora{

    valor;
    resultado;

    calcularSeno(){
        this.resultado=Math.sin(this.valor*Math.PI/180);
        return this.resultado;
    }

    calcularCoseno(){
        this.resultado=Math.cos(this.valor*Math.PI/180);
        return this.resultado;
    }

    calcularTangente(){
        this.resultado=Math.tan(this.valor*Math.PI/180);
        return this.resultado;
    }

    

}
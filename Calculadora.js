class Calculadora{
     
    num1;
    num2;
    resultado;

    sumar(){

        this.resultado=this.num1+this.num2;
        return this.resultado;

    }

    restar(){

        this.resultado=this.num1-this.num2;
        return this.resultado;
    }

    multiplicar(){

        this.resultado=this.num1*this.num2;
        return this.resultado;
    }

    dividir(){

        this.resultado=this.num1/this.num2;
        return this.resultado;
    }

    modulo(){
        this.resultado=this.num1%this.num2;
        return this.resultado;
    }
}
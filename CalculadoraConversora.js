class CalculadoraConversora extends CalculadoraCientifica{

    

    gradosARadianes(){
        this.resultado=this.valor*(Math.PI/180);
        return this.resultado;

    }

    radianesAGrados(){
        this.resultado=this.valor*(180/Math.PI);
        return this.resultado;

    }

    kelvinACentigrados(){
        this.resultado=this.valor-273.15;
        return this.resultado;


    }

    centigradosAKelvin(){
        this.resultado=this.valor+273.15;
        return this.resultado;


    }
}